# Great Transformations: Social revolutions erupted during energy transitions around the World, 1500-2013.

Companion repository for publication "Great Transformations: Social revolutions erupted during energy transitions around the World, 1500-2013."

This repository contains all code and documentation to reproduce:

1) [Preprocessing and analysis](01_preprocessing.Rmd) to generate the SI data file [data_file.xlsx](data_file.xlsx) that includes all result data
2) generation of the [figures and tables](02_figures_tables.Rmd) used in the manuscript
3) generation of the [SI figures and tables](03_figures_tables_SI.Rmd) reported in the supplementary information

## Licensing

- [![License: CC BY-NC-ND 4.0](https://licensebuttons.net/l/by-nc-nd/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/): all tables and figures
- [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0): all software code in this repository

The code was developed for the Potsdam Insitute for Climate Impact Research by Peter Paul Pichler. For attribution please use DOI:XXX
